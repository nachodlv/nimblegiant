// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "NimbleGiant/Structs/FPlayerScore.h"
#include "GameFramework/GameModeBase.h"
#include "NimbleGiantGameMode.generated.h"

UCLASS(minimalapi)
class ANimbleGiantGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANimbleGiantGameMode();
protected:
	virtual void BeginPlay() override;

private:
	TSubclassOf<class APyramidGenerator> PyramidGenerator;
	UPROPERTY()
	APyramidGenerator* PyramidGeneratorRef;


	UFUNCTION()
	void AllCubesDestroyed();
};



