﻿#pragma once
#include "FPlayerScore.generated.h"

USTRUCT()
struct FPlayerScore
{
    GENERATED_BODY()

    FString Name;
    int32 Score;

    FPlayerScore(const FString NewName, const int NewScore): Name(NewName), Score(NewScore){}
    FPlayerScore(): Name(""), Score(0){}
};
