// Copyright Epic Games, Inc. All Rights Reserved.

#include "NimbleGiant.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NimbleGiant, "NimbleGiant" );
 