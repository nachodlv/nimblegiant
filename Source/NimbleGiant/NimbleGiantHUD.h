// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "NimbleGiant/Structs/FPlayerScore.h"
#include "NimbleGiantHUD.generated.h"

UCLASS()
class ANimbleGiantHUD : public AHUD
{
	GENERATED_BODY()

public:
	ANimbleGiantHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

protected:
	virtual void BeginPlay() override;

private:
	FString Score = "Score: 0";
	/** Crosshair asset pointer */
	UPROPERTY()
	class UTexture2D* CrosshairTex;
	TArray<FPlayerScore> PlayerScores = TArray<FPlayerScore>();
	bool ShowTable;

	UPROPERTY()
	UFont* Font;

	UFUNCTION()
	void UpdateScore(int NewScore);
	UFUNCTION()
	void StartShowingTable(const TArray<FPlayerScore>& PlayerScoresToShow);

	void DrawTable();

};

