// Copyright Epic Games, Inc. All Rights Reserved.

#include "NimbleGiantHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "CanvasItem.h"
#include "NimbleGiantCharacter.h"
#include "Engine/Classes/Engine/Font.h"
#include "UObject/ConstructorHelpers.h"

ANimbleGiantHUD::ANimbleGiantHUD()
{
    static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(
        TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
    CrosshairTex = CrosshairTexObj.Object;

    static ConstructorHelpers::FObjectFinder<UFont> FontObj(TEXT("/Game/Fonts/NimbleGiantFont"));
    if (!FontObj.Object)
    {
        UE_LOG(LogTemp, Warning, TEXT("Cant find font"));
    }
    Font = FontObj.Object;
}


void ANimbleGiantHUD::DrawHUD()
{
    Super::DrawHUD();

    if (ShowTable)
    {
        DrawTable();
        return;
    }

    // Draw very simple crosshair

    // find center of the Canvas
    const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

    // offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
    const FVector2D CrosshairDrawPosition((Center.X),
                                          (Center.Y + 20.0f));

    // draw the crosshair
    FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
    TileItem.BlendMode = SE_BLEND_Translucent;
    Canvas->DrawItem(TileItem);

    DrawText(Score, FLinearColor::Black, 0, 0, Font);
}

void ANimbleGiantHUD::StartShowingTable(const TArray<FPlayerScore>& PlayerScoresToShow)
{
    ShowTable = true;
    PlayerScores = PlayerScoresToShow;
}

void ANimbleGiantHUD::BeginPlay()
{
    if (ANimbleGiantCharacter* Character = Cast<ANimbleGiantCharacter>(GetOwningPawn()))
    {
        Character->OnScoreChanged.AddDynamic(this, &ANimbleGiantHUD::UpdateScore);
        Character->OnShowTable.AddDynamic(this, &ANimbleGiantHUD::StartShowingTable);
    }
}

void ANimbleGiantHUD::UpdateScore(const int NewScore)
{
    Score = FString::Printf(TEXT("Score: %d"), NewScore);
}

void ANimbleGiantHUD::DrawTable()
{
    DrawText(FString(TEXT("Game Over")), FColor::Black, Canvas->ClipX * 0.5f, Canvas->ClipY*0.1f, Font);
    FVector2D StartingPosition = FVector2D(Canvas->ClipX * 0.3f, Canvas->ClipY * 0.25f);
    const float NameXPosition = Canvas->ClipX * 0.4f;
    const float ScoreXPosition = Canvas->ClipX * 0.75f;
    for (int32 i = 0; i < PlayerScores.Num(); i++)
    {
        FPlayerScore PlayerScore = PlayerScores[i];

        // Draw position, name and score of player
        DrawText(FString::Printf(TEXT("%d."), i + 1), FColor::Black, StartingPosition.X, StartingPosition.Y, Font);
        DrawText(PlayerScore.Name, FColor::Black, NameXPosition, StartingPosition.Y, Font);
        DrawText(FString::FromInt(PlayerScore.Score), FColor::Black, ScoreXPosition, StartingPosition.Y, Font);

        StartingPosition.Y += Canvas->ClipY * 0.1f;
    }
}
