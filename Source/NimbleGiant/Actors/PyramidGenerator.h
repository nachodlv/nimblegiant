// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "Engine/Public/Net/UnrealNetwork.h"
#include "PyramidGenerator.generated.h"

class ACube;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAllCubesDestroyed);

UCLASS()
class NIMBLEGIANT_API APyramidGenerator : public AActor
{
    GENERATED_BODY()

public:
    FAllCubesDestroyed OnAllCubesDestroyed;

    APyramidGenerator();
    virtual void Tick(float DeltaTime) override;
    void GeneratePyramid(int Height, int WidthBase);

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pyramid Properties", meta = (AllowPrivateAccess = "true"))
    TArray<FColor> PossibleColors;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cube Settings", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<ACube> Cube;

    int32 CubesQuantity = 0;

    void InstantiateCube(FVector Location, FRotator Rotation) const;
    FColor GetRandomColor() const;
    UFUNCTION()
    void CubeDestroyed(AActor* DestroyedActor);
};
