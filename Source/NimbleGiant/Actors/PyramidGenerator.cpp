// Fill out your copyright notice in the Description page of Project Settings.


#include "PyramidGenerator.h"
#include "Cube.h"

APyramidGenerator::APyramidGenerator()
{
    bReplicates = true;
    PrimaryActorTick.bCanEverTick = false;
}

void APyramidGenerator::GeneratePyramid(int Height, int WidthBase)
{
    const FVector ActorLocation = GetActorLocation();
    const FRotator ActorRotation = GetActorRotation();
    const float CubeSideLength = ACube::GetSize();
    for (auto i = 0; i < Height; i++)
    {
        for (auto j = 0; j < WidthBase - i; j++)
        {
            const float Z = ActorLocation.Z + i * CubeSideLength;
            const float Y = ActorLocation.Y + j * CubeSideLength + i * CubeSideLength / 2;
            InstantiateCube(FVector(ActorLocation.X, Y, Z), ActorRotation);
            CubesQuantity++;
        }
    }
}

void APyramidGenerator::InstantiateCube(FVector Location, FRotator Rotation) const
{
    ACube* NewCube = GetWorld()->SpawnActor<ACube>(Cube->GetDefaultObject()->GetClass(), Location, Rotation);
    NewCube->SetColor(GetRandomColor());
    NewCube->OnDestroyed.AddDynamic(this, &APyramidGenerator::CubeDestroyed);
}

FColor APyramidGenerator::GetRandomColor() const
{
    const float RandomIndex = FMath::RandRange(0, PossibleColors.Num() - 1);
    return PossibleColors[RandomIndex];
}

void APyramidGenerator::CubeDestroyed(AActor* DestroyedActor)
{
    CubesQuantity--;
    if(CubesQuantity == 0) OnAllCubesDestroyed.Broadcast();
}

// Called every frame
void APyramidGenerator::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}
