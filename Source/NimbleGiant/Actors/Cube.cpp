// Fill out your copyright notice in the Description page of Project Settings.


#include "Cube.h"
#include "DrawDebugHelpers.h"
#include "NimbleGiant/NimbleGiantProjectile.h"
#include "NimbleGiant/NimbleGiantCharacter.h"
#include "Components/BoxComponent.h"
#include "Net/UnrealNetwork.h"

const float ACube::CubeSide = 100.f;

ACube::ACube()
{
    PrimaryActorTick.bCanEverTick = false;

    bReplicates = true;

    // Set up box component and collisions
    BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
    SetRootComponent(BoxComponent);
    if(GetLocalRole() == ROLE_Authority) BoxComponent->OnComponentHit.AddDynamic(this, &ACube::OnComponentHit);
    BoxComponent->SetSimulatePhysics(true);
    BoxComponent->SetBoxExtent(FVector(CubeSide/2, CubeSide/2, CubeSide/2));

    // Set up static mesh
    BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Capsule Component"));
    BaseMesh->SetupAttachment(RootComponent);

}

void ACube::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(ACube, CubeColor);
}

float ACube::GetSize()
{
    return CubeSide;
}

void ACube::BeginPlay()
{
    Super::BeginPlay();
}

void ACube::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ACube::SetColor(const FColor NewColor)
{
    CubeColor = NewColor;
    UMaterialInstanceDynamic* Material = UMaterialInstanceDynamic::Create(BaseMesh->GetMaterial(0), this);
    Material->SetVectorParameterValue(FName(TEXT("Color")), CubeColor);
    BaseMesh->SetMaterial(0, Material);
}

void ACube::GetNeighbours(TArray<ACube*>& OutCubes) const
{
    TArray<FHitResult> SweepResult;
    const FVector ActorLocation = GetActorLocation();
    const FQuat ActorRotation = GetActorRotation().Quaternion();
    const FCollisionShape Shape = FCollisionShape::MakeBox(FVector(BoxTraceSize, BoxTraceSize, BoxTraceSize));
    GetWorld()->SweepMultiByChannel(SweepResult, ActorLocation, ActorLocation, ActorRotation, ECC_Visibility, Shape);

    for (FHitResult& Result : SweepResult)
    {
        if (ACube* Cube = Cast<ACube>(Result.GetActor()))
        {
            OutCubes.Add(Cube);
        }
    }
}

void ACube::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                           FVector NormalImpulse, const FHitResult& Hit)
{
    if(!HasAuthority()) return;
    if (ANimbleGiantProjectile* Projectile = Cast<ANimbleGiantProjectile>(OtherActor))
    {
        const int FinalScore = Fibonacci(HitCube(true));
        if(ANimbleGiantCharacter* Character = Projectile->GetCharacterOwner())
        {
            Character->AddScore(FinalScore);
        }
        Destroy();
    }
}

void ACube::OnRep_CubeColor()
{
    UMaterialInstanceDynamic* Material = UMaterialInstanceDynamic::Create(BaseMesh->GetMaterial(0), this);
    Material->SetVectorParameterValue(FName(TEXT("Color")), CubeColor);
    BaseMesh->SetMaterial(0, Material);
}

int ACube::HitCube(bool FirstCube)
{
    if (WasHit) return 0;
    int CubesHit = 0;
    WasHit = true;
    TArray<ACube*> Neighbours;
    GetNeighbours(Neighbours);
    for (auto Neighbour : Neighbours)
    {
        if (Neighbour != nullptr && Neighbour->CubeColor == CubeColor)
        {
            CubesHit += Neighbour->HitCube(false);
        }
    }
    if(!FirstCube) Destroy();
    return 1 + CubesHit;
}

float ACube::Fibonacci(const int N)
{
    if(N <= 0) return 0;
    if(N == 1) return 1;
    return Fibonacci(N-1) + Fibonacci(N-2);
}

