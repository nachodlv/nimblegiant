// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cube.generated.h"

class UBoxComponent;
UCLASS()
class NIMBLEGIANT_API ACube : public AActor
{
    GENERATED_BODY()

public:
    ACube();
    virtual void Tick(float DeltaTime) override;
    void SetColor(const FColor NewColor);
    static float GetSize();

protected:
    virtual void BeginPlay() override;


private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* BaseMesh;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components", meta = (AllowPrivateAccess = "true"))
    UBoxComponent* BoxComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Box Trace", meta = (AllowPrivateAccess = "true"))
    float BoxTraceSize = 200.f;

    UPROPERTY(ReplicatedUsing=OnRep_CubeColor, EditAnywhere, BlueprintReadWrite, Category="Color", meta = (AllowPrivateAccess = "true"))
    FColor CubeColor = FColor::Red;

    const static float CubeSide;
    bool WasHit = false;

    UFUNCTION()
    void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                        FVector NormalImpulse, const FHitResult& Hit);
    UFUNCTION()
    void OnRep_CubeColor();
    void GetNeighbours(TArray<ACube*>& OutCubes) const;
    int HitCube(bool FirstCube);
    static float Fibonacci(int N);
};
