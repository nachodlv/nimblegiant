// Copyright Epic Games, Inc. All Rights Reserved.

#include "NimbleGiantGameMode.h"

#include "NimbleGiantCharacter.h"
#include "NimbleGiantHUD.h"
#include "Engine/World.h"
#include "NimbleGiant/Actors/PyramidGenerator.h"
#include "GameFramework/PlayerState.h"
#include "NimbleGiant/Structs/FPlayerScore.h"
#include "UObject/ConstructorHelpers.h"

ANimbleGiantGameMode::ANimbleGiantGameMode()
    : Super()
{
    // set default pawn class to our Blueprinted character
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(
        TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
    DefaultPawnClass = PlayerPawnClassFinder.Class;
    static ConstructorHelpers::FClassFinder<APyramidGenerator> PyramidGeneratorFinder(
        TEXT("/Game/Blueprints/BP_PyramidGenerator"));
    PyramidGenerator = PyramidGeneratorFinder.Class;

    // use our custom HUD class
    HUDClass = ANimbleGiantHUD::StaticClass();
}

void ANimbleGiantGameMode::BeginPlay()
{
    Super::BeginPlay();
    if (UWorld* World = GetWorld())
    {
        PyramidGeneratorRef = World->SpawnActor<APyramidGenerator>(PyramidGenerator.GetDefaultObject()->GetClass(),
                                                                   FVector(0, 100, 220), FRotator::ZeroRotator);
        if (!PyramidGeneratorRef) { return; }
        PyramidGeneratorRef->GeneratePyramid(7, 7);
        PyramidGeneratorRef->OnAllCubesDestroyed.AddDynamic(this, &ANimbleGiantGameMode::AllCubesDestroyed);
    }
}

void ANimbleGiantGameMode::AllCubesDestroyed()
{
    int32 Index = 0;
    FConstControllerIterator Iterator = GetWorld()->GetControllerIterator();
    TArray<FPlayerScore> PlayerScores = TArray<FPlayerScore>();
    for (; Iterator; ++Iterator)
    {
        AController* Controller = Iterator->Get();
        PlayerScores.Add(FPlayerScore(FString::Printf(TEXT("Player: %d"), Index + 1),
                                      Controller->PlayerState->GetScore()));
        Index++;
    }
    PlayerScores.Sort([](const FPlayerScore& LeftScore, const FPlayerScore& RightScore) -> bool
    {
        return LeftScore.Score > RightScore.Score;
    });
    Iterator.Reset();
    for (; Iterator; ++Iterator)
    {
        if (ANimbleGiantCharacter* Character = Cast<ANimbleGiantCharacter>(Iterator->Get()->GetCharacter()))
        {
            Character->ShowTable(PlayerScores);
        }
    }
}
